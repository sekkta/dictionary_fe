import { createRouter, createWebHashHistory } from 'vue-router'
import App from './App.vue';

import publicTribe from './components/tribes/publicTribe.vue'
import logIn from './components/user/logIn.vue'
import home from './components/home.vue'
import word from './components/word/wordContent.vue'
import wordType from './components/wordType/wordTypeContent.vue'
import tribe from './components/tribes/tribeContent.vue'
import user from './components/user/userContent.vue'
import language from './components/language/languageContent.vue'
import createWord from './components/word/createWord.vue'


const routes = [{
        path: '/',
        name: 'publicTribe',
        component: publicTribe
    },
    {
        path: '/logIn',
        name: 'logIn',
        component: logIn
    },
    {
        path: '/home',
        name: 'home',
        component: home
    },
    {
        path: '/language',
        name: 'language',
        component: language
    },

]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router