import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import menuLateral from './components/menuLateral.vue'
import wordIndex from './components/word/wordContent.vue'
import languageIndex from './components/language/languageContent.vue'
import tribeIndex from './components/tribes/tribeContent.vue'
import userIndex from './components/user/userContent.vue'
import wordTypeIndex from './components/wordType/wordTypeContent.vue'

import {
    faSignInAlt,
    faUsers,
    faSignOutAlt,
    faList,
    faListAlt,
    faUsersCog,
    faLanguage,
    faPlusCircle,
    faHome,
    faEdit,
    faTrashAlt,
    faTimesCircle,
    faEnvelope
} from '@fortawesome/free-solid-svg-icons'

library.add({
    faSignInAlt,
    faUsers,
    faSignOutAlt,
    faList,
    faListAlt,
    faUsersCog,
    faLanguage,
    faPlusCircle,
    faHome,
    faEdit,
    faTrashAlt,
    faTimesCircle,
    faEnvelope
})

createApp(App).
use(router).
use(ElementPlus).
component('icon', FontAwesomeIcon).
component('menuLateral', menuLateral).
component('WordIndex', wordIndex).
component('languageIndex', languageIndex).
component('tribeIndex', tribeIndex).
component('userIndex', userIndex).
component('WordTypeIndex', wordTypeIndex).
mount('#app')